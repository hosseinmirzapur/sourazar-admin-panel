import Vue from 'vue'
import { ToastPlugin, ModalPlugin } from 'bootstrap-vue'
import VueCompositionAPI from '@vue/composition-api'
import Ripple from 'vue-ripple-directive'

import VueSweetalert2 from 'vue-sweetalert2'
import router from './router'
import store from './store'
import App from './App.vue'
import CKEditor from '@ckeditor/ckeditor5-vue2'

// If you don't need the styles, do not connect
// import 'sweetalert2/dist/sweetalert2.min.css'
// Global Components
import './global-components'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/toastification'
import '@/@core/scss/vue/libs/vue-select.scss'
import '@/libs/state'
import '@/libs/globalFunctions'
import '@/libs/globalConstants'
import '@/libs/axios'
import axios from "axios"
import VueSimpleAlert from "vue-simple-alert"
Vue.directive('ripple', Ripple)
axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`
// BSV Plugin Registration
Vue.use(ToastPlugin)
Vue.use(VueSimpleAlert)
Vue.use(ModalPlugin)
Vue.use(VueSweetalert2)
Vue.use( CKEditor )
// Composition API
Vue.use(VueCompositionAPI)

// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')

// impost Yekan font
require('@/assets/font/Font-Yekan.css')

Vue.config.productionTip = false

axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'
// main.js
import VuePersianDatetimePicker from 'vue-persian-datetime-picker'
Vue.use(VuePersianDatetimePicker, {
  name: 'custom-date-picker',
  props: {
    format: 'jYYYY-jMM-jDD',
    editable: false,
    placeholder: 'تاریخ مورد نظر را وارد کنید...',
    altFormat: 'YYYY-MM-DD',
    color: '#44405e',
    autoSubmit: false,
    clearable: true,
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
