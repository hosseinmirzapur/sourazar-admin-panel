import Vue from 'vue'
import VueRouter from 'vue-router'
import data from '@/navigation/vertical/index'

Vue.use(VueRouter)

const that = Vue.prototype

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    //  Dashboard
    {
      path: '/profile',
      name: 'profile',
      component: () => import('@/views/Profile.vue'),
      meta: {
        pageTitle: 'پروفایل ادمین',
        breadcrumb: [
          {
            text: 'پروفایل',
            active: true
          }
        ]
      }
    },
    {
      path: '/',
      name: 'Dashboard',
      component: () => import('@/views/Dashboard/Home.vue'),
      meta: {
        pageTitle: 'داشبورد',
        breadcrumb: [
          {
            text: 'داشبورد',
            active: true,
            to: '/'
          }
        ]
      },
    },
    //  Users
    {
      path: '/users',
      name: 'users',
      component: () => import('@/views/Users/Users.vue'),
      meta: {
        pageTitle: 'کاربران سایت',
        breadcrumb: [
          {
            text: 'لیست کاربران',
            active: true,
          }
        ],
      },
    },
    {
      path: '/users/:id',
      name: 'show-user',
      component: () => import('@/views/Users/users-view/UsersView.vue'),
      meta: {
        pageTitle: 'کاربران سایت',
        breadcrumb: [
          {
            text: 'لیست کاربران',
            active: false,
            to: '/users',
          },
          {
            text: 'مشاهده کاربر',
            active: true,
          },
        ],
      },
    },
    // panel
    {
      path: '/admin-accounts',
      name: 'admin-accounts',
      component: () => import('@/views/Admins/AdminAccounts.vue'),
      meta: {
        pageTitle: 'مدیران سایت',
        breadcrumb: [
          {
            text: 'مدیریت سامانه',
            active: true,
          },
          {
            text: 'لیست مدیران',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin-permissions',
      name: 'admin-permissions',
      component: () => import('@/views/Permissions/PermissionList')
    },
    // dollar
    {
      path: '/dollar',
      name: 'dollar',
      component: () => import('@/views/DollarPrice.vue'),
      meta: {
        pageTitle: 'تاریخچه قیمت دلار',
        breadcrumb: [
          {
            text: 'مدیریت دلار',
            active: true,
          },
        ],
      },
    },
    {
      path: '/cryptocurrency',
      name: 'cryptocurrency',
      component: () => import('@/views/CryptoCurrency/CryptoCurrency.vue'),
      meta: {
        pageTitle: 'لیست رمز ارزها',
        breadcrumb: [
          {
            text: 'مدیریت رمز ارزها',
            active: true,
          },
          {
            text: 'لیست رمز ارز',
            active: true,
          },
        ],
      },
    },
    {
      path: '/cryptocurrency/:id',
      name: 'edit-cryptocurrency',
      component: () => import('@/views/CryptoCurrency/CryptoEdit.vue'),
      meta: {
        pageTitle: 'ویرایش رمز ارز',
        breadcrumb: [
          {
            text: 'لیست رمز ارزها',
            active: false,
            to: '/cryptocurrency',
          },
          {
            text: 'ویرایش رمز ارز',
            active: true,
          },
        ],
      },
    },
    // Orders
    {
      path: '/orders',
      name: 'orders',
      component: () => import('@/views/Orders/Orders.vue'),
      meta: {
        pageTitle: 'لیست سفارشات',
        breadcrumb: [
          {
            text: 'سفارشات',
            active: true,
          },
        ],
      },
    },
    {
      path: '/order-details/:id',
      name: 'order-details',
      component: () => import('@/views/Orders/OrderDetails.vue'),
      meta: {
        pageTitle: 'جزییات سفارش',
        breadcrumb: [
          {
            text: 'سفارشات',
            active: true,
          },
        ],
      },
    },
    // toman transactions
    {
      path: '/toman-transactions',
      name: 'toman-transactions',
      component: () => import('@/views/CryptoWalletAddress/CryptoWalletAddresses.vue'),
      meta: {
        pageTitle: 'مدیریت تراکنش های تومانی',
        breadcrumb: [
          {
            text: 'تراکنش های تومانی',
            active: true,
          },
        ],
      }
    },
    {
      path: '/bank-transactions',
      name: 'bank-transactions',
      component: () => import('@/views/BankTransactions.vue'),
      meta: {
        pageTitle: 'مدیریت تراکنش های بانکی',
        breadcrumb: [
          {
            text: 'همه تراکنش های انجام شده',
            active: true
          }
        ]
      }
    },
    //  support
    {
      path: '/support',
      name: 'support',
      component: () => import('@/views/Support/Chat.vue'),
      meta: {
        pageTitle: 'پشتیبانی',
        breadcrumb: [
          {
            text: 'چت',
            active: true,
          },
        ],
      },
    },
    //  FaQ
    {
      path: '/faq',
      name: 'faq',
      component: () => import('@/views/Faq/Faq.vue'),
      meta: {
        pageTitle: 'سوالات متداول',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    //  site service
    {
      path: '/site-service',
      name: 'site-service',
      component: () => import('@/views/Faq/Faq.vue'),
      meta: {
        pageTitle: 'سوالات متداول',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    //  setting
    {
      path: '/privacy_policies',
      name: 'privacy_policies',
      component: () => import('@/views/Setting/PrivacyPolicies/PrivacyPolicies.vue'),
      meta: {
        pageTitle: 'قوانین و مقررات',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: () => import('@/views/Setting/AboutUs/AboutUs.vue'),
      meta: {
        pageTitle: 'درباره ما',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    {
      path: '/contact-us',
      name: 'contact-us',
      component: () => import('@/views/Setting/ContactUs/ContactUs.vue'),
      meta: {
        pageTitle: 'تماس با ما',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    {
      path: '/social-network',
      name: 'social-network',
      component: () => import('@/views/Setting/SocialNetwork/SocialNetwork.vue'),
      meta: {
        pageTitle: 'شبکه های اجتماعی',
        breadcrumb: [
          {
            text: 'لیست',
            active: true,
          },
        ],
      },
    },
    //  Authentication
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
      meta: {
        layout: 'full',
        no_auth: true,
      },
    },
    {
      path: '/error-404',
      name: 'error-404',
      component: () => import('@/views/error/Error404.vue'),
      meta: {
        layout: 'full',
        no_auth: true,
      },
    },
    {
      path: '*',
      redirect: 'error-404',
    },
  ]
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen

router.beforeEach(async (to, from, next) => {
  if (that.state.token && !that.state.gotten) {
    that.state.loading = true
    await that.$getUserInfo()
    // await that.$filter(sourceData)
    that.state.menuItem = JSON.parse(JSON.stringify(data))
  }
  if (to.meta.no_auth && that.state.token) next('/')
  else if ((to.meta.no_auth && !that.state.token) || (!to.meta.no_auth && that.state.token)) next()
  else if (!to.meta.no_auth && !that.state.token) next('/login')
})
router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

export default router
