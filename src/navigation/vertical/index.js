export default [
  {
    title: 'داشبورد',
    route: 'Dashboard',
    icon: 'HomeIcon',
    // role: ['all'],
  },
  {
    title: 'کاربران سایت',
    route: 'users',
    icon: 'UsersIcon',
    // role: ['all'],
  },
  //  Admin and Roles and Permissions
  {
    title: 'مدیریت سامانه',
    icon: 'MonitorIcon',
    // role: ['all'],
    children: [
      {
        title: 'ادمین ها',
        route: 'admin-accounts',
        // role: ['all'],
      }
    ],
  },
  //  Currency
  {
    title: 'مدیریت دلار',
    route: 'dollar',
    icon: 'DollarSignIcon',
    // role: ['all'],
  },
  //  CryptoCurrency
  {
    title: 'مدیریت رمز ارز ها',
    route: 'cryptocurrency',
    tagVariant: 'light-info',
    // tag: 'new',
    icon: 'CpuIcon',
    // role: ['all'],
  },
  //    Orders
  {
    title: 'سفارشات',
    route: 'orders',
    // tagVariant: 'light-info',
    icon: 'ShoppingCartIcon',
    // role: ['all'],
  },
  //    wallet address
  {
    title: ' مدیریت تراکنش های تومانی',
    route: 'toman-transactions',
    icon: 'ShoppingBagIcon',
    // role: ['all'],
  },
  {
    title: 'مدیریت تراکنش های بانکی',
    route: 'bank-transactions',
    icon: 'ZapIcon'
  },
  {
    title: 'پشتیبانی',
    route: 'support',
    icon: 'MessageSquareIcon',
    // role: ['all'],
  },
  {
    title: 'سوالات متداول',
    route: 'faq',
    icon: 'AlignJustifyIcon',
    // role: ['all'],
  },
  {
    title: 'تنظیمات سایت',
    route: 'setting',
    icon: 'SettingsIcon',
    // role: ['all'],
    children: [
      {
        title: 'قوانین و مقررات',
        route: 'privacy_policies',
        // role: ['all'],
      },
      {
        title: 'درباره ما',
        route: 'about-us',
        // role: ['all'],
      },
      {
        title: 'تماس با ما',
        route: 'contact-us',
        // role: ['all'],
      },
      {
        title: 'لینک شبکه اجتماعی',
        route: 'social-network',
        // role: ['all'],
      },
    ],
  },
]
