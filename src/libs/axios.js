import Vue from 'vue'

// axios
import axios from 'axios'
import router from "@/router"

const that = Vue.prototype

axios.defaults.baseURL = 'https://api.exchang-az.com/public/api'
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.headers.get['Authorization'] = `Bearer ${localStorage.getItem('token')}`
axios.defaults.headers.post['Authorization'] = `Bearer ${localStorage.getItem('token')}`
axios.defaults.headers.common['Content-Type'] = 'application/json'

axios.interceptors.response.use(
    res => {
      that.state.loading = false
      return res
    },
    async err => {
      that.state.loading = false

      if (err.response.status) {
        if (err.response.status === 401) {
          localStorage.clear()
          this.state.token = ''
          this.state.gotten = false
          // console.log(err.response.status)
          await router.push('/login')
          that.$swal({
            icon: 'error',
            title: ' هویت کاربر نامشخص!',
            text: 'هویت شما نامشخص است لطفا از قسمت ورود به پنل وارد پنل شوید!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        }
        else if (err.response.status === 400) {
          that.$swal({
            icon: 'error',
            title: 'خطایی رخ داده است!',
            text: 'عملیات ناموفق!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        }
        else if (err.response.status === 403) {
          await router.push('/')
          that.$swal({
            icon: 'error',
            title: 'خطای شبکه!',
            text: 'متاسفانه در این لحظه در سمت سرور مشکلی رخ داده است!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        } else if (err.response.status === 500) {
          that.$swal({
            icon: 'error',
            title: 'خطای شبکه!',
            text: 'متاسفانه در این لحظه در سمت سرور مشکلی رخ داده است!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        } else if (err.response.status === 404) {
          that.$swal({
            icon: 'error',
            title: 'خطایی رخ داده است!',
            text: 'این عملیات قابل انجام نیست!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        } else if (err.response.status === 422) {
          that.$swal({
            icon: 'error',
            title: 'خطایی رخ داده است!',
            text: 'اطلاعات وارد شده ناقص میباشد!',
            confirmButtonText: 'متوجه شدم',
            customClass: {
              confirmButton: 'btn btn-success',
            },
          })
        }
      }

      return Promise.reject(err)
    },)

that.$axios = axios

export default axios



// const axiosIns = axios.create({
//   // You can add your headers here
//   // ================================
//   baseURL: 'http://127.0.0.1:8000/api/',
//   // timeout: 1000,
//   // headers: {'X-Custom-Header': 'foobar'}
// })
//
// Vue.prototype.$http = axiosIns
//
// export default axiosIns

// const that = Vue.prototype
